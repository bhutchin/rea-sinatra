# README #

### Design Choices ###

* Ansible:
	* decentralisation of the deployment to allow for no single point of failure 
	* fast spin up times for end to end deployments with minimal interaction
	* these ansible tasks should be expanded to allow for better use of variables to allow it to be more modular for deployments of other apps. I have started to expand this with use of group_vars but I believe it should be further extended into the use of tag setups in the aws ec2 launchs
* Docker 
	* The use of docker to build the app environment itself was predominantly to allow for consistent builds that are agnostic of what the server is built off. I would have liked to allow for multiple build profiles and to have an option to build the dockerfile on the local machine so as to test it out and is definitly something I would look to do in further revisions.

### Prerequisites ###

* AWS configuration
    * This script assumes you hav preconfigured key pairs from aws labaled aws_launch instructions to configure a key pair can be found [here](https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair)
    * This ansible deployment is configured to read the keys from: `~/.ssh/aws_key.pem` however this can be updated from within the inventory files or in the ansible.cfg file as explained [here](https://docs.ansible.com/ansible/2.4/intro_configuration.html#private-key-file)
* Ansible binaries need to be installed as can be found for your OS here:
    * https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
* Boto python package installed `pip install boto`
* Boto configured this was completed with both the config and credentials files in `~/.aws/` as outlined [here](http://docs.pythonboto.org/en/latest/boto_config_tut.html)
* This repo will need to be cloned to be able to run the configuration commands

#### How to get started ####

Assuming all prerequisites have been fulfilled 

Clone the repo

	$ git clone https://bhutchin@bitbucket.org/bhutchin/rea-sinatra.git
	Cloning into 'rea-sinatra'...

Enter the directory and run the ansible binary
	
	$ cd rea-sinatra
	$ ansible-playbook -i ec2.py playbooks/main.yml -K
	
This should request the become password configured for the deploy user which is setup in the task 'setup initial deploy user' The current hash is set to `rea-sinatra` but should be updated

This should run through all of the tasks defined in playbooks/main.yml

At the end of the run you should see a recap for each host that has tasks run against it. This run should see two hosts listed localhost which is used to launch the ec2 instance and then the single ec2 instance that has been launched and then configured 

	PLAY RECAP
	************************************************************************************************************************************
	localhost           : ok=6    changed=1    unreachable=0    failed=0
	52.65.170.190       : ok=13   changed=10   unreachable=0    failed=0		
	

### Limitations ###

* This deployment on its own does not include any recovery or continued setups to confirm that everything is setup and must be manually run with the current setup
* I have found that some slow ec2 startup times have caused ansible to not match any hossts as the ec2 nistance wont have started in time to be read into the other tasks. TO fix this I did attempt to extend the wait time but was stilling having issues. Due to ansible decalring the desired state of the machine it allows this to be run multiple times to fix anything that may have failed in a first run or may have been changed after the first run and needs to be reverted back.

### Further changes ###

These are the changes I would like to include in further revisions to automate the build process and further secure it.

* IP restrictions for SSH access
* Build profiles into boto to help with building staging and prod environments
* allow for a local build option that will test the dockerfile locally to allow it to be tested before requiring cloud setups
* Tests to confirm that web app has started correctly a simple check on port 80 for a http 200 code would be a starting point
* reverse proxy to allow for more web apps to be deployed to the same server as well as SSL certs and 80 -> 443 redirection 

